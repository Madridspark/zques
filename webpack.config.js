var webpack = require("webpack");
var appName = "zques";

module.exports =
{
    entry: 
    [
        'webpack/hot/only-dev-server',
        "./app/router.jsx"
    ],
    output:
    {
        path: "./dist/js",
        filename: "bundle.js"
    },
    module:
    {
        loaders:
        [
            {
                test: /\.jsx?$/,
                loaders:
                [
                    "react-hot",
                    "babel?presets[]=react,presets[]=es2015"
                ],
                exclude: /node_modules/
            },
            {
                test: /\.js$/,
                loader: "babel-loader",
                exclude: /node_modules/
            },
            {
                test: /\.less$/,
                loaders:
                [
                    "style",
                    "css?modules&importLoaders=1&localIdentName="+ appName + "--[local]-[hash:base64:5]",
                    "less"
                ]
            }
        ]
    },
    resolve:
    {
        extensions:['','.js','.jsx', '.less']
    },
    plugins:
    [
      new webpack.HotModuleReplacementPlugin()
    ]
};