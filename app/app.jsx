import React from "react";
import Styles from "./style.less";

export default class App extends React.Component
{
    render()
    {
        var SubPage = React.Children.map(

            this.props.children,
            function (child)
            {
                return <section>{child}</section>;
            }
        );
        return(
            
            <div>
                <header>首页</header>
                <div>{SubPage}</div>
            </div>
        );
    }
}