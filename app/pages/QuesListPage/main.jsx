import React from "react";
import QuesList from "../../components/QuesList/main";
import Styles from "./style.less";

export default class ListPage extends React.Component
{
    render()
    {

        return(

            <div>
                <div id={Styles["ques-list-container"]}><QuesList /></div>
            </div>
        )
    }
}

