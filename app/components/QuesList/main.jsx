import React from "react";
import Styles from "./style.less";
import Moment from "moment";
// Components
import ListItem from "./ListItem/main";
import CheckBox from "../Checkbox/main";
import TipWindow from "../TipWindow/main";

Moment.locale('zh-cn');

export default class List extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state =
        {
            checkAll: false,
            multiDelete: false,
            tipEnable: false,
            tipText: "",
            deletedItems: []
        };

        this.ListItems =
        [ 
            {
                id: 0,
                name: "问卷一",
                time: null,
                state: 0
            }, 
            {
                id: 1,
                name: "问卷二",
                time: "2016年10月23日16:21:41",
                state: 1
            },
            {
                id: 2,
                name: "问卷三",
                time: Moment("2014年10月2日16:21:41", ChFormat).format(ChFormat),
                state: 2
            },
            {
                id: 3,
                name: "问卷三",
                time: Moment("2014年10月2日16:21:41", ChFormat).format(ChFormat),
                state: 2
            },
            {
                id: 4,
                name: "问卷三",
                time: Moment("2014年10月2日16:21:41", ChFormat).format(ChFormat),
                state: 2
            },
            {
                id: 5,
                name: "问卷三",
                time: Moment("2014年10月2日16:21:41", ChFormat).format(ChFormat),
                state: 2
            },
            {
                id: 6,
                name: "问卷三",
                time: Moment("2014年10月2日16:21:41", ChFormat).format(ChFormat),
                state: 2
            },
            {
                id: 7,
                name: "问卷三",
                time: Moment("2014年10月2日16:21:41", ChFormat).format(ChFormat),
                state: 2
            }
        ];
    }

    render()
    {
        var bottomBar;
        var itemArr = [];
        if(true)
        {
            bottomBar = <tr style={{border: "none"}}>
                            <td colSpan="5">
                                <CheckBox type="checkbox" checked={this.state.checkAll} onClick={(e) => this.checkAllHandler(e)} />
                                <span  className={Styles["check-all"]}>
                                    全选
                                </span>
                                <button className={Styles["btn-active"] + " " + Styles["btn-delete-all"]} disabled={!this.state.multiDelete} onClick={e => this.multiDeleteHandler(e)}>
                                    删除
                                </button>
                            </td>
                        </tr>;
        }

        for(var i = 0; i < this.ListItems.length; i++)
        {
            itemArr.push(
                
                <ListItem key={i} value={this.ListItems[i]} ref={i.toString()} checkHandler={(isChecked) => this.toglleDeleteAll(isChecked)} deleteHandler={this.deleteHandler} deleteFunction={this.deleteItems.bind(this)}/>
            );
        }

        return(
            <div className={Styles.list}>
                <TipWindow enabled={this.state.tipEnable} confirm={() => this.confirmDeleteItems()} toggle={() => this.toglleTipWindow()} caption="提示" text={this.state.tipText} />
                <table>
                    <thead>
                        <tr>
                            <th></th>
                            <th>标题</th>
                            <th>时间</th>
                            <th>状态</th>
                            <th>操作</th>
                        </tr>
                        <button className={Styles["btn-new"] + " " + Styles["btn-active"]}>＋新建问卷</button>
                    </thead>
                    <tbody>
                        {itemArr}
                    </tbody>
                    {bottomBar}
                </table>
            </div>
        );
    }

    deleteHandler(e)
    {
        this.props.deleteFunction(this.props.value.id);
    }

    multiDeleteHandler(e)
    {
        this.deleteItems();
    }

    deleteItems(id)
    {
        var deletedItems = [];
        var deleteTip = "";

        if(id !== undefined)
        {
            deletedItems.push(id);

            for(var i = 0; i < this.ListItems.length; i++)
            {
                if(this.ListItems[i].id == id)
                {
                    deleteTip = '确定删除"' + this.ListItems[i].name + '"？';
                }
            }
        }
        else
        {
            for(var i = 0; i < this.ListItems.length; i++)
            {
                if(this.refs[i].state.isChecked)
                {
                    deletedItems.push(this.ListItems[i].id);
                }
            }

            deleteTip = "确定删除这" + deletedItems.length + "项？";
        }

        this.setState(
            {
                deletedItems: deletedItems,
                tipText: deleteTip
            },
            () => this.toglleTipWindow()
        );
    }

    confirmDeleteItems()
    {
        alert("请求服务器，删除：" + this.state.deletedItems);

        this.toglleTipWindow();
    }

    checkAllHandler(e)
    {
        this.setState(
            {checkAll: !this.state.checkAll}
        );

        for(var i = 0; i < this.ListItems.length; i++)
        {
            var _this = this.refs[i];

            _this.setState(
                {isChecked: e.target.checked},
                function()
                {
                    return _this.checkHandler();
                }
            );
        }
    }

    toglleDeleteAll(isChecked)
    {
        if(isChecked)
        {
            for(var i = 0; i < this.ListItems.length; i++)
            {
                if(!this.refs[i].state.isChecked)
                {
                    this.setState({multiDelete: isChecked });
                    return;
                }
            }
        }
        else
        {
            for(var i = 0; i < this.ListItems.length; i++)
            {
                if(this.refs[i].state.isChecked)
                {
                    this.setState({checkAll: isChecked});
                    return;
                }
            }
        }

        this.setState({ checkAll: isChecked, multiDelete: isChecked });
    }

    toglleTipWindow()
    {
        this.state.tipEnable ? 
            this.setState({ deletedItems: [], tipEnable: false })
        :
            this.setState({tipEnable: true});
            
    }
}