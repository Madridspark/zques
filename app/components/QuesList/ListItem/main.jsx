import React from "react";
import Styles from "./style.less";
import Moment from "moment";
// components
import CheckBox from "../../Checkbox/main";

Moment.locale('zh-cn');

export default class ListItem extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state =
        {
            isChecked: false
        }
    }

    render()
    {
        var btnArr = <td style={{textAlign: "right"}}>
                         <button>编辑</button>
                         <button onClick={this.props.deleteHandler.bind(this)}>删除</button>
                         <button>结果</button>
                     </td>;

        var stateCode, stateText, stateClass;
        stateCode = this.props.value.state || 0;
        switch(stateCode)
        {
            case 0:
                {
                    stateText = "已结束";
                    stateClass = Styles.published;
                    break;
                }
            case 1:
                {
                    stateText = "发布中";
                    stateClass = Styles.publishing;
                    btnArr = <td style={{textAlign: "right"}}>
                                 <button onClick={this.props.deleteHandler.bind(this)}>删除</button>
                                 <button>结果</button>
                             </td>;
                    break;
                }
            case 2:
                {
                    stateText = "未发布";
                    btnArr = <td style={{textAlign: "right"}}>
                                 <button>编辑</button>
                                 <button onClick={this.props.deleteHandler.bind(this)}>删除</button>
                             </td>;
                    stateClass = null;
                    break;
                }
        }

        return (
        <tr>
            <td><CheckBox onClick={e => this.checkboxClickHandler(e)} checked={this.state.isChecked} value={this.props.value.id}/></td>
            <td>{this.props.value.name || "未命名"}</td>
            <td>{this.props.value.time || "未知时间"}</td>
            <td className={stateClass}>{stateText}</td>
            {btnArr}
        </tr>
        );
    }

    checkboxClickHandler(e)
    {
        this.setState(
                {isChecked: !this.state.isChecked},
                () => this.checkHandler()
            );
    }

    checkHandler()
    {
        this.props.checkHandler(this.state.isChecked);
    }
};

ListItem.propTypes =
{
    checkHandler: React.PropTypes.func.isRequired,
    deleteHandler: React.PropTypes.func.isRequired,
    deleteFunction: React.PropTypes.func.isRequired
}