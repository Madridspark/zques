import React from "react";
import Styles from "./style.less";

export default class CheckBoxCom extends React.Component
{
    render()
    {
        return (

            <label className={Styles["checkbox-com"]}>
                <input type="checkbox" onClick={this.props.onClick} checked={this.props.checked} value={this.props.value}/>
                <div></div>
            </label>
        )
    }
}

CheckBoxCom.propTypes =
{
    checked: React.PropTypes.bool.isRequired,
    onClick: React.PropTypes.func.isRequired
}