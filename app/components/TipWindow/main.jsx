import React from "react";
import Styles from "./style.less";

export default class TipWindow extends React.Component
{
    render()
    {
        var classArr = Styles["tip-window-mask"];

        if(this.props.enabled)
        {
            classArr += " " + Styles["tip-window-enabled"];
        }

        return(

            <div className={classArr} onClick={this.props.toggle}>
                <div className={Styles["tip-window"]} onClick={this.windowClickHandler}>
                    <div className={Styles["tip-window-caption"]}>
                        {this.props.caption}
                        <div onClick={this.props.toggle}>X</div>
                    </div>
                    <div className={Styles["tip-window-contant"]}>
                        <p>{this.props.text}</p>
                    </div>
                    <div className={Styles["tip-window-oparations"]}>
                        <button onClick={this.props.confirm}>确定</button>
                        <button onClick={this.props.toggle}>取消</button>
                    </div>
                </div>
            </div>
        );
    }

    windowClickHandler(e)
    {
        e.stopPropagation();
    }
}

TipWindow.propTypes =
{
    enabled: React.PropTypes.bool.isRequired,
    caption: React.PropTypes.string,
    text: React.PropTypes.string.isRequired,
    confirm: React.PropTypes.func.isRequired,
    toggle: React.PropTypes.func.isRequired
}

TipWindow.defaultProps =
{
    caption: "提示"
}