import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, Link, IndexRoute, browserHistory } from 'react-router';
import Styles from "./style.less";

// Pages
import App from "./app";
import ListPage from "./pages/QuesListPage/main";

ReactDOM.render
(
    <Router history={browserHistory}>
        <Route path="/" component={App}>
            <IndexRoute component={ListPage}/>
            <Route path="haha" component={ListPage} />
        </Route>
    </Router>,
    document.getElementById('root-container')
);